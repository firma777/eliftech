import React from "react";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

const PaginationComponent = (props) => {
   const PaginationContent = ()=> {
      const content = [];
      if (props.countButtons > 0){
         for (let i=1; i <= props.countButtons; i++){
            if (props.activeButton == i){
               content.push(
                     <PaginationItem key={i} active>
                        <PaginationLink href="#">
                           {i}
                        </PaginationLink>
                     </PaginationItem>
               );
            } else {
               content.push(
                     <PaginationItem key={i}>
                        <PaginationLink href="#" onClick={()=> props.clickHandler(i)}>
                           {i}
                        </PaginationLink>
                     </PaginationItem>
               );
            }
         }
      }
      return (content);
   }

   if (props.countButtons > 0){
      return (
            <Pagination>
               <PaginationContent/>
            </Pagination>
      );
   } else {
      return(
         <div></div>
      );
   }
}

export default PaginationComponent;

