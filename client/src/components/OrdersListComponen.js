import React from "react";
import PaginationComponent from "./PaginationComponent";
import {Table, Button, Input, FormGroup, CustomInput, Label, Alert, Row, Col} from "reactstrap";

class OrdersList extends React.Component {
   constructor(props){
      super(props);
      this.state = {
         activePage: 1,
         pages: 0,
         ordersToShow: [], 
         fileToUpload: null,
         sortField: "date",
         sortOrder: 1,
         alertShow: false,
      }
      this.clickHandler = this.clickHandler.bind(this);
      this.onFileUpload = this.onFileUpload.bind(this);
      this.onFileSelected = this.onFileSelected.bind(this);
      this.handleSort = this.handleSort.bind(this);
      this.onDismis = this.onDismis.bind(this);
   }

   clickHandler(page){
      this.fetchOrders(page);
   }

   fetchOrders(page){
      fetch(`http://localhost:3001/orders/list?page=${page}&sortField=${this.state.sortField}&sortOrder=${this.state.sortOrder}`, {method: "GET"})
      .then(
         response => {
            if (response.ok){
               return response;
            } else {
               var error = new Error("Error " + response.status + ": " + response.statusText);
               throw error;
            }
         }
      )
      .then(
         response => {
            return response.json();
         }
      )
      .then(
         response => {
            this.setState({ordersToShow: response.orders, pages: response.pages, activePage:page});
            console.log(this.state);
         }
      )
   }

   onFileSelected(event){
      this.setState({fileToUpload: event.target.files[0]})
   }

   onFileUpload(){
      if(this.state.fileToUpload != null){
         const data = new FormData();
         data.append("csvfile", this.state.fileToUpload);
         fetch("http://localhost:3001/orders/csv", {method: "POST", body: data}).then(
            res => {
               this.setState({alertShow: true, fileToUpload: null }, ()=> {
                  this.fetchOrders(this.state.activePage)
                  document.getElementById("fileToUpload").value = null;
                  console.log(res.body);
               });
            }
         )
      }
   }

   handleSort(sortField){
      let sortOrder = this.state.sortOrder;
      sortOrder = this.state.sortField == sortField ? -this.state.sortOrder : this.state.sortOrder;
      this.setState({sortField: sortField, sortOrder}, ()=>{
         this.fetchOrders(this.state.activePage);
      });
   }

   onDismis(){
      this.setState({alertShow: false});
   }
   
   componentDidMount(){
      this.fetchOrders(1);
   }

   render(){
      if (this.state.ordersToShow.length == 0){
         return(
            <Table size="sm" hover responsive striped>
               <thead>
                  <tr>
                     <th ><a onClick={() => this.handleSort("id")} href="#">Order ID</a></th>
                     <th ><a onClick={() => this.handleSort("email")} href="#">User email</a></th>
                     <th ><a onClick={() => this.handleSort("date")} href="#">Date</a></th>
                     <th ><a onClick={() => this.handleSort("price")} href="#">Price</a></th>
                     <th ><a onClick={() => this.handleSort("currency")} href="#">Currency</a></th>
                     <th ><a onClick={() => this.handleSort("status")} href="#">Status</a></th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td colSpan="6">
                        There is nothing to show
                     </td>
                  </tr>
               </tbody>
            </Table>
         );
      } else {
         let OrderListContent = () => {
            let content = this.state.ordersToShow.map( order => {
               return(
                  <tr key={order._id}>
                     <td>{order._id}</td>
                     <td>{order.email}</td>
                     <td>{order.date}</td>
                     <td>{order.price}</td>
                     <td>{order.currency}</td>
                     <td>{order.status}</td>
                  </tr>
               )
            });
            return content;
         }
         return(
            <React.Fragment>
               <Table size="sm" hover responsive striped>
                  <thead>
                     <tr>
                     <th ><a onClick={() => this.handleSort("id")} href="#">Order ID</a></th>
                     <th ><a onClick={() => this.handleSort("email")} href="#">User email</a></th>
                     <th ><a onClick={() => this.handleSort("date")} href="#">Date</a></th>
                     <th ><a onClick={() => this.handleSort("price")} href="#">Price</a></th>
                     <th ><a onClick={() => this.handleSort("currency")} href="#">Currency</a></th>
                     <th ><a onClick={() => this.handleSort("status")} href="#">Status</a></th>
                     </tr>
                  </thead>
                  <tbody>
                     <OrderListContent/>
                  </tbody>
               </Table>
               <PaginationComponent countButtons={this.state.pages} activeButton={this.state.activePage} clickHandler={this.clickHandler} />
               <br/><br/>
               <Row>
                  <Col>
                     <a className="btn btn-primary" href="http://localhost:3001/orders/csv" role="button">Download CSV report</a>
                  </Col>
                  <Col>
                     <FormGroup>
                        <CustomInput ref={this.fileInput} onChange={this.onFileSelected} type="file" name="csvfile" accept=".csv" id="fileToUpload"/>
                        <br/><br/>
                        <Button color="primary" onClick={this.onFileUpload}>Upload CSV file</Button>
                     </FormGroup>
                     <Alert isOpen={this.state.alertShow} toggle={this.onDismis} color="success">File was successfully uploaded</Alert>
                  </Col>
               </Row>
            </React.Fragment>
         );
      }
   }
}

export default OrdersList;