import React from 'react';
import OrdersList from "./components/OrdersListComponen"
import './App.css';

class App extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return (
      <div className="App">
        <OrdersList/>
      </div>
    )
  }
}

export default App;
