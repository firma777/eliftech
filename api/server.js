const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();
const ordersRouter = require("./routers/ordersRouter");
const bodyParser = require("body-parser");
const port = 3001;
const host = "localhost";
const MongoClient = require('mongodb').MongoClient;
const dbUrl = "mongodb://localhost:27017";
const dbName = 'mydatabase';
let db;

app.use(bodyParser.json());
app.use(fileUpload({
   createParentPath: true,
   limits: { fileSize: 1 * 1024 * 1024 } // 1MB
}));

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
 });

MongoClient.connect(dbUrl, { useUnifiedTopology: true },  (err, client) => {
   if (err) {
      return console.log("Something is wrong with dataabase connection");
   } else {
      db = client.db(dbName);

      app.use('/orders', ordersRouter(db));

      app.listen(port, host, () => {
         console.log("\n***************************************");
         console.log("Database connection has been establishe");
         console.log("Express server started on port: ", port);
         console.log("***************************************");
      })
   }
})