const ObjectID = require('mongodb').ObjectID;
const router = require('express').Router();
const csvtojson = require("csvtojson");
const json2csvParser = require("json2csv").Parser;
const pageSize = 12;
const dateHelper = require("date-and-time");
const datePattern = dateHelper.compile("DD.MM.YYYY HH:mm:ss");
const months = [
  'Zero-month', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
]

const ordersRouter = (db) => {
  let orders = db.collection("orders");

  router.get('/list', function(req, res) {
    let page = req.query.page || 1;
    let sortField = req.query.sortField || "email";
    let sortOrder = +req.query.sortOrder || 1;
    let response = {
      pages: 0,
      orders: []
    };

    orders.find().count(
      (e, count) => {
        response.pages = Math.ceil(count / pageSize);
      }
    );
    orders.find().skip( (page * pageSize) - pageSize ).limit(pageSize).sort({[sortField]: sortOrder}).toArray().then(function(result){
      response.orders = result;
      response.orders = response.orders.map(order=>{
        return {...order, date: dateHelper.format(order.date, "DD.MM.YYYY HH:mm:ss")}
      });
      res.send(response);
    });
  });

  router.get('/csv', function(req, res) {
    let fields = {fields: ["email", "date", "amount"]};
    const parser = new json2csvParser(fields);
    orders.aggregate(
        [
          {$match: {status: "approved"}},
          {$project: {
            email: "$email",
            month: {$month: "$date"},
            year: {$year: "$date"},
            price: "$price"
          }},
          {$group: {
            _id: {email: "$email", month: "$month", year: "$year"},
            amount: {$sum: "$price"}
          }}
        ]
      ).toArray().then(
        function(result){
          const fixedResult = [];
          result.forEach(function(doc){
            fixedResult.push({
              email: doc._id.email,
              date: months[doc._id.month] + " " + doc._id.year.toString(),
              amount: doc.amount
            });
          });
          const csv = parser.parse(fixedResult);
          res.contentType('text/csv');
          res.setHeader('content-disposition', 'attachment; filename=' + "orders-" + new Date().getTime() + ".csv");
          res.send(csv);
        }
    )
  });

  router.post('/csv', function(req, res) {
    if(req.files){
      let csvFile = req.files.csvfile;
      console.log(csvFile);
      csvFile.mv("./uploads/" + csvFile.name)

      csvtojson()
      .fromFile("./uploads/" + csvFile.name)
      .then(csvData => {
        let csvFixed = []
        csvData.forEach(function(doc){
          doc.price = +doc.price;
          doc.date = dateHelper.parse(doc.date, datePattern);
          csvFixed.push(doc);
        })

        orders.insertMany(csvFixed, ()=> {
          res.send(csvFixed);
        }) 
      })
    }else{
      res.send('There was no file in the request');
    }
  });

  return router;
}

module.exports = ordersRouter;